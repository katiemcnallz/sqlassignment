-- 1. Show all information about the bond with the CUSIP '28717RH95'.

select * from bond where CUSIP = '28717RH95';

-- 2. Show all information about all bonds, in order from shortest (earliest maturity) to longest (latest maturity).

select * from bond order by maturity ASC;

-- 3. Calculate the value of this bond portfolio, as the sum of the product of each bond's quantity and price.

select sum(quantity * price) from bond


-- 4. Show the annual return for each bond, as the product of the quantity and the coupon. Note that the coupon rates are quoted as whole percentage points, so to use them here you will divide their values by 100.

select quantity*coupon/100  from bond


-- 5. Show bonds only of a certain quality and above, for example those bonds with ratings at least AA2. (Don't resort to regular-expression or other string-matching tricks; use the bond-rating ordinal.)

select * from rating, bond where rating.rating = 'AAA'or rating.rating = 'AA1' or rating.rating='AA2'

-- 6. Show the average price and coupon rate for all bonds of each bond rating.

select AVG(price), AVG(coupon), bond.rating from bond join rating on bond.rating = rating.rating group by bond.rating

-- 7. Calculate the yield for each bond, as the ratio of coupon to price. Then, identify bonds that we might consider to be overpriced, as those whose yield is less than the expected yield given the rating of the bond.

select b.id, b.cusip, coupon/price yield, r.expected_yield from bond b join rating r on r.rating = b.rating where (coupon/price) < r.expected_yield
