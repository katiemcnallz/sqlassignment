--Find all trades by a given trader on a given stock - for example the trader with ID=1 and the ticker 'MRK'. This involves joining from trader to position and then to trades twice, through the opening- and closing-trade FKs.

select case when b.buy = 1 then 'BUY' else 'SELL' end, b.size, b.price
from trade b
  join position a on a.opening_trade_id = b.id or a.closing_trade_id = b.id
where a.trader_id = 2 and b.stock = 'AAPL'
order by b.instant;

--Find the total profit or loss for a given trader over the day, as the sum of the product of trade size and price for all sales, minus the sum of the product of size and price for all buys.

select sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end))
from trade t
  join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 2
  and t.stock = 'MRK'
  and p.closing_trade_id != p.opening_trade_id
  and p.closing_trade_id is not null;

--Develop a view that shows profit or loss for all traders.
